
from setuptools import setup, find_packages
# To use a consistent encoding
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
      name='tas_public',
      
      version='0.1.2',
      
      description='Accompanying files and resources for the These Are Systems newsletter',
      
      long_description=long_description,
      
      url='https://gitlab.com/these-are-systems/public',
      
      
      author='Jacob Bayless, P. Eng',
      author_email='jacob@jbaylessconsulting.ca',
      
      license='MIT',
      
      classifiers=[
           'Development Status :: 3 - Alpha',
            'Intended Audience :: Developers',
            'Topic :: Scientific/Engineering',
            'License :: OSI Approved :: MIT License',
            'Programming Language :: Python :: 3',
      ],
      
      # What does your project relate to?
    keywords='data analysis',
    
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    
    install_requires=['matplotlib',
                      'numpy',
                      'gif',
                      'ffmpy'],
    
    extras_require={},
      
      )
