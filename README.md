These Are Systems
=================

Here you can find source code to accompany selected newsletter articles from *These Are Systems*.

*These Are Systems* is the substack newsletter of Jacob Bayless Consulting Inc.
You can read *These Are Systems* here: https://jbconsulting.substack.com/

This is the README file for the project.

----

Installation instructions:

Most scripts should run without any need for installation, as long as the prerequisite packages are available.

1) Clone the git repository at https://gitlab.com/these-are-systems/public 
2) In the folder where the project has been cloned, run "python setup.py develop"

Use of conda envs is always encouraged, especially for Windows users.
