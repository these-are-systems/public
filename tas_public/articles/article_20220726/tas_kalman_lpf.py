'''
Created on July. 26, 2022
These Are Systems

Copyright © 2022 Jacob Bayless Consulting Inc
Author: Jacob Bayless, P. Eng


Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the “Software”), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

"""
This file contains the source code accompanying the newsletter article here:
https://jbconsulting.substack.com/p/is-the-kalman-filter-just-a-low-pass
"""

import os
import numpy as np

import matplotlib
matplotlib.use("webAgg")
from matplotlib import pyplot as plt

from scipy.signal import butter, sosfilt

rng = np.random.default_rng()

class Temperature_System(object):
    def __init__(self, T0 = 20, Q = 1, R = 1,
                 initial_kalman_mean = 20,
                 initial_kalman_cov = 1,
                 initial_wiener_val = 20,
                 timestep_s = 1):
        """
        Create a Temperature System instance
        
        Parameters:
        ----------
        T0 : float, optional
            Initial temperature of the system (Celsius, although it doesn't matter)
            Default is 20
        Q : float, optional
            The process noise covariance, in degrees-squared-per-timestep
            Default is 1
        R : float, optional
            The measurement noise covariance, in degrees-squared
            Default is 1
        initial_kalman_mean : float, optional
            The initialization value of the Kalman filter mean
            Defualt is 20
        initial_kalman_cov : float, optional
            The initialization value of the Kalman filter covariance
        initial_wiener_val : float, optional
            The initializatoin value of the Wiener filter state
        timestep_s : float, optional
            The time, in seconds, between subsequent observations
        
        Additional keyword arguments passed to _init_bayes() to initialize
        the Bayes filter.
        """
        self.timestep = timestep_s
        self.Q = Q # Process noise
        self.R = R # Measurement noise
        self.T_array = [T0]
        self.T_meas_array = []
        
        # Kalman filter
        self.kalman_mean_array = [initial_kalman_mean]
        self.kalman_mean_forecast_array = [np.NaN]
        self.kalman_cov_array = [initial_kalman_cov]
        self.kalman_cov_forecast_array = [np.NaN]
        self.kalman_gain_array = [np.NaN]
        
        # Wiener filter
        self.K_ss = 1./(1 + (2*self.R/(Q + np.sqrt(self.Q*(self.Q + 4*self.R)))))
        self.wiener_array = [initial_wiener_val]
        
        # Initial state measurement
        self.measure()
    
    def step(self, num_steps = 1):
        for _ in range(num_steps):
            T_old = self.T_array[-1]
            T_new = T_old + rng.normal(loc = 0.0, scale = self.Q)
            self.T_array.append(T_new)
            
            self.kalman_predict()
            T_meas = self.measure()
            self.kalman_update(T_meas)
            self.wiener_filter(T_meas)
            
    def measure(self):
        """
        Measure a new data point
        
        Returns
        -------
        T_meas : float
            The measured temperature
        """
        
        T_noise = rng.normal(loc = 0.0, scale = self.R)
        T_meas = self.T_array[-1] + T_noise
        self.T_meas_array.append(T_meas)
        
        return T_meas
    
    def kalman_predict(self):
        """
        The prediction step for the optimal Kalman filter for this system
        """ 
        # The system is unitary (F = 1) so there's no better prediction than the current guess
        mean_forecast = self.kalman_mean_array[-1]
        # But our uncertainty grows a bit, due to the process noise
        cov_forecast = self.kalman_cov_array[-1] + self.Q
        self.kalman_mean_forecast_array.append(mean_forecast)
        self.kalman_cov_forecast_array.append(cov_forecast)
    
    def kalman_update(self, T_meas):
        """
        The update step for the optimal Kalman filter for this system
        
        Parameters
        ----------
        T_meas : float
            The measured temperature
        """ 
        # Kalman mean (forecast)
        x_k_forecast = self.kalman_mean_forecast_array[-1]
        # Kalman covariance (forecast
        P_k = self.kalman_cov_forecast_array[-1]
        # Observation matrix
        H_k = 1
        # Measurement noise
        R_k = self.R
        # Kalman gain
        K_k = P_k*H_k / ((H_k*P_k*H_k) + R_k)
        
        # Update mean and covariance
        x_k1 = x_k_forecast + K_k*(T_meas - H_k*x_k_forecast)
        cov_k1 = (1.0 - K_k*H_k)*P_k*(1.0 - K_k*H_k) + K_k*R_k*K_k
        
        
        # Update our arrays for plotting
        self.kalman_gain_array.append(K_k)
        self.kalman_mean_array.append(x_k1)
        self.kalman_cov_array.append(cov_k1)
    
    def wiener_filter(self, T_meas):
        """
        The optimal IIR Wiener filter for this system has the transfer function
        G(z) = 1/(1 + (z - 1)/K)
        
        This code implements that transfer function.
        
        Parameters
        ----------
        T_meas : float
            The measured temperature
        
        Returns
        -------
        T_filt : float
            The filtered temperature measurement
        """ 
        
        T_filt_prev = self.wiener_array[-1]
        T_filt = T_filt_prev + self.K_ss*(T_meas - T_filt_prev)
        
        self.wiener_array.append(T_filt)
        return T_filt
    
    def plot_filter_comparison(self, fig = None):
        
        time = np.arange(len(self.T_meas_array))*self.timestep
        
        if fig is None:
            fig = plt.figure(figsize = (9, 4))
            ax_temp = fig.add_axes([0.1, 0.125, 0.85, 0.8])
            ax_temp.grid()
            ax_temp.set_ylabel("Temperature (°C)")
            ax_temp.set_xlabel("Time (s)")
            ax_temp.set_title("The Kalman filter becomes indistinguishable from the Wiener (low-pass) filter after ~10 samples")
            ax_temp.set_xlim(time[0]-2, time[-1])
        else:
            (fig, ax_temp) = fig
        
        
        ax_temp.plot(time, self.T_array, color = "seagreen", lw = 3, label = "Truth", zorder = 2)
        ax_temp.errorbar(time, self.T_meas_array, yerr = np.sqrt(self.R), ls = "None", label = "Data",
                         color = "darkseagreen", alpha = 0.5, marker = "+", zorder = 1)
        ax_temp.plot(time, self.kalman_mean_array, ls = "-", color = "cornflowerblue", label = "Kalman filter",
                     lw = 3, zorder = 3)
        
        k_min = np.array(self.kalman_mean_array) - np.sqrt(self.kalman_cov_array)
        k_max = np.array(self.kalman_mean_array) + np.sqrt(self.kalman_cov_array)
        
        ax_temp.fill_between(time, k_min, k_max, color = "cornflowerblue", alpha = 0.25, zorder = 4)
        
        ax_temp.plot(time, self.wiener_array, ls = "--", color = "gold", label = "Wiener filter", zorder = 5)
        ax_temp.legend(loc = "lower right")
    
    
    def plot_kalman_gain(self, fig = None):
        time = np.arange(len(self.kalman_gain_array))*self.timestep
        
        if fig is None:
            fig = plt.figure(figsize = (9, 2))
            ax_K = fig.add_axes([0.1, 0.25, 0.85, 0.5])
            ax_K.grid()
            ax_K.set_ylabel("Kalman gain")
            ax_K.set_xlabel("Time (s)")
            ax_K.set_title("The Kalman gain rapidly converges to a constant")
            ax_K.set_xlim(time[0]-2, time[-1])
            ax_K.set_ylim(0, 1)
        else:
            (fig, ax_K) = fig
            
        ax_K.plot(time, self.kalman_gain_array, ls = "-", color = "cornflowerblue", lw = 2,
                  label = "Kalman gain")
        
        ax_K.plot(time, np.ones(time.shape)*self.K_ss, ls = "--", color = "black",
                  label = "Steady-state")
        
        ax_K.legend(loc = "upper right")
    
    def bode_plot(self, fig = None):
        """
        Plot the Wiener filter (steady-state Kalman filter) frequency spectrum
        """
        dt = self.timestep
        pole_w = np.exp(-(1 - self.K_ss)*dt)
        pole_f = pole_w/(2*np.pi)
        f_nyquist = 0.5*dt
        w = np.logspace(np.log10(0.01*pole_w), np.log10(2*np.pi*f_nyquist), 1024, dtype = np.complex128)
        
        w_hz = w/(2*np.pi)
        z = np.exp(1J*w*dt)
        TF = 1.0/(1.0 + (z - 1)/self.K_ss)
        
        if fig is None:
            fig = plt.figure(figsize = (9, 2))
            ax_mag = fig.add_axes([0.1, 0.25, 0.85, 0.5])
            ax_mag.grid()
            ax_mag.set_ylabel("Kalman gain")
            ax_mag.set_xlabel("Frequency (Hz)")
            ax_mag.set_title("The Kalman gain rapidly converges to a constant")
            ax_mag.set_xscale("log")
            ax_mag.set_yscale("log")
            ax_mag.set_title("Bode plot of the steady-state Kalman filter")
            ax_mag.set_ylabel("Magnitude")
            ax_mag.set_xlim(w_hz[0], w_hz[-1])
            ax_mag.set_ylim(0.1, 10)
        else:
            (fig, ax_mag) = fig
        
        ax_mag.plot(w_hz, np.ones(w_hz.shape), lw = 1, color = "black", ls = "--")
        ax_mag.plot(w_hz, np.abs(TF), lw = 2, color = "cornflowerblue", label = "Steady-state Kalman filter")
        
        ax_mag.plot([pole_f, pole_f], [0.1, 10], lw = 1, color = "grey", ls = "--")

def lpf_hpf_demo():
    """
    Demonstrates the idea of low-pass and high-pass filtering
    """
    samples = 500
    t = np.linspace(0, 10, samples)
    dt = t[1] - t[0]
    # Generate some nice random-looking data
    dc_offset = 20
    y_raw = dc_offset + np.cumsum(rng.normal(loc = 0, scale = 1.0, size = t.shape)) + rng.normal(loc = 0.0, scale = 2.0, size = t.shape)
    bw_lpf = butter(2, 1, btype = "lowpass", output = "sos", fs = 1./dt)
    bw_hpf = butter(2, 1, btype = "highpass", output = "sos", fs = 1./dt)
    
    
    y_lpf, _ = sosfilt(bw_lpf, y_raw, zi = [[dc_offset, -0.9*dc_offset]])
    y_hpf, _ = sosfilt(bw_hpf, y_raw, zi = [[-dc_offset, dc_offset]])
    
    
    fig = plt.figure(figsize = (9, 3))
    ax = fig.add_axes([0.1, 0.15, 0.85, 0.75])
    ax.grid()
    ax.set_xlabel("Time (s)")
    ax.set_ylabel("Value")
    ax.plot(t, y_raw, label = "raw data", color = "black")
    ax.plot(t, y_lpf, label = "low-pass filtered", color = "goldenrod", lw = 2)
    ax.plot(t, y_hpf, label = "high-pass filtered", color = "mediumseagreen")
    ax.legend()
    

if __name__ == "__main__":
    print("Start!")
    my_beaker = Temperature_System(R = 0.75,
                                   Q = 0.1,
                                   initial_kalman_mean = 15,
                                   initial_kalman_cov = 10,
                                   initial_wiener_val = 15)
    my_beaker.step(250)
    my_beaker.plot_filter_comparison()
    my_beaker.plot_kalman_gain()
    my_beaker.bode_plot()
    lpf_hpf_demo()
    plt.show()
    print("Done.")
