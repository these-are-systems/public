'''
Created on Jun. 7, 2022
These Are Systems

Copyright © 2022 Jacob Bayless Consulting Inc
Author: Jacob Bayless, P. Eng


Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the “Software”), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

"""
This file contains the source code accompanying the newsletter article here:
https://jbconsulting.substack.com/p/basics-updating-from-data
"""

import os
import numpy as np

import matplotlib
matplotlib.use("webAgg")
from matplotlib import pyplot as plt
import gif


rng = np.random.default_rng()


# Replace this with the path to your local ffmpeg executable if you want mp4 output
ffmpeg_path = None

def normalize_log(probability):
    """
    Normalize a log probability distribution so that all probabilities sum to one
    
    Parameters
    ----------
    probability : numpy.array
        An array of log-probabilities
    
    Returns
    ----------
    probability : numpy.array
        Normalized array of log-probabilities
    """
    return (probability - np.log(np.sum(np.exp(probability))))

class Lighthouse(object):
    def __init__(self,
                 a_actual = 0.0,
                 b_actual = 1.0,
                 num_a_points = 201,
                 num_b_points = 100,
                 left_shoreline_extent_km = -100.,
                 right_shoreline_extent_km = 100.,
                 offshore_extent_km = 100.):
        """
        Create a Lighthouse problem instance
        
        Parameters
        ----------
        a_actual : float, optional
            The true location of the lighthouse along the shoreline, in km
            Default is 0 (the origin)
        b_actual : float, optional
            The true location of the lighthouse along the shoreline, in km
            Default is 1 km offshore
        num_a_points : int, optional
            The number of grid points to use for discretizing the shoreline
            Default is 201
        num_b_points : int, optional
            The number of grid points to use for discretizing the ocean
            Default is 100
        left_shoreline_extent_km : float, optional
            Extent of the shoreline grid to the left, in km
            Default is -100
        right_shoreline_extent_km : float, optional
            Extent of the shoreline grid to the right, in km
            Default is 100
        offshore_extent_km : float, optional
            Extent of the ocean grid offshore, in km
            Default is 100
        """
        self.a_actual = a_actual
        self.b_actual = b_actual
        self.observations = []
        
        self.a_min = left_shoreline_extent_km
        self.a_max = right_shoreline_extent_km
        self.b_min = 0
        self.b_max = offshore_extent_km
        a = np.linspace(self.a_min, self.a_max, num_a_points, endpoint = True)
        b = np.linspace(self.b_min, self.b_max, num_b_points, endpoint = False)
        b += 0.5*b[1]
    
        aa, bb = np.meshgrid(a, b, indexing = 'ij')
        
        self.a = a
        self.b = b
        self.aa = aa
        self.bb = bb
        
        # Jeffreys prior over b, uniform over a
        log_prior = normalize_log(-np.log(self.bb))
        self.log_probability = log_prior
        
        self.y_axis_label = "Distance into the sea (km)"
        self.y_axis_title = "Probability for β (distance offshore)"
        self.generate_figure()
    
    def generate_data(self):
        """
        Generate a new observed light flash along the shoreline at a random position
        
        Returns
        -------
        x_n : float
            Position of the light flash, in kilometres
        """
        theta = rng.uniform(-np.pi, np.pi)
        x_n = self.a_actual + self.b_actual*np.tan(theta)
        self.observations.append(x_n)
        return(x_n)
    
    def update(self, x_n):
        """
        Update the probability distribution based on a single new observation
        
        Parameters
        ----------
        x_n : float
            Position of the new light flash, in kilometres
        """
        
        log_likelihood = np.log((1./np.pi)*(self.bb/(self.bb**2 + (x_n - self.aa)**2)))
        log_prior = self.log_probability
        log_posterior =  log_likelihood + log_prior
        self.log_probability = np.nan_to_num(log_posterior, copy = False, nan = -np.inf)
        self.log_probability = normalize_log(self.log_probability)
        
    def feed_data(self, num_data_points = 1):
        """
        Generate a number of observations and update the probability distribution
        accordingly.
        
        Parameters
        ----------
        num_data_points : int
            The requested number of new observations to update on
        """
        if num_data_points == 0:
            return
        for _ in range(int(num_data_points)):
            x_n = self.generate_data()
            self.update(x_n)
        
    def generate_figure(self):
        self.fig = plt.figure(figsize = (8,6))
        self.gs = self.fig.add_gridspec(3, 2, width_ratios = (2, 8), height_ratios = (2, 4, 1),
                                        hspace = 0.4, wspace = 0.3)
        
        self.ax_img = self.fig.add_subplot(self.gs[1,1])
        self.ax_a = self.fig.add_subplot(self.gs[0,1])
        self.ax_b = self.fig.add_subplot(self.gs[1,0])
        self.ax_data = self.fig.add_subplot(self.gs[2,1])
        
        self.ax_img.set_xlim((self.a_min, self.a_max))
        self.ax_img.set_ylim((self.b_min, self.b_max))
        
        self.ax_a.set_xlim((self.a_min, self.a_max))
        self.ax_b.set_ylim((self.b_min, self.b_max))
        
        # Remove unneeded axis tick labels
        for ax in [self.ax_a, self.ax_b, self.ax_data]:
            ax.set_xticklabels([])
            ax.set_yticklabels([])
            ax.set_xticks([])
            ax.set_yticks([])

        self.ax_data.set_xlabel("Observed light flashes (raw data)")
        self.ax_img.set_xlabel("Position along shoreline (km)")
        self.ax_img.set_ylabel(self.y_axis_label)
        self.ax_img.set_title("Estimated lighthouse location (joint probability)")
        self.ax_a.set_title("Probability for α (shoreline location)")
        self.ax_b.set_ylabel(self.y_axis_title)
    
    def plot_probabilities(self, show_actual = True, animated = False, **kwargs):
        if animated:
            # gif library seems to require the figure is regenerated each frame
            self.generate_figure()
        
        probability = np.exp(self.log_probability)
        probability_max = np.max(probability)
        
        # Plot the joint probability distribution
        self.ax_img.pcolormesh(self.aa, self.bb, probability,
                                    vmin = 0.0, vmax = probability_max,
                                    shading = 'gouraud')
        
        if show_actual:
            # Mark the true lighthouse position with a white circle
            self.ax_img.scatter([self.a_actual], [self.b_actual], marker = 'o',
                                edgecolors = "white", facecolors  = "none")
        
        # Get the 1-d probability distribution over shoreline position
        p_a_sum = np.nansum(probability, axis = 1)
        self.ax_a.plot(self.a, p_a_sum, color = "darkgreen", label = "Marginal distribution")
        self.ax_a.fill_between(self.a, np.zeros(self.a.shape), p_a_sum, color = "darkgreen", alpha = 0.25)
        
        self.ax_a.plot([self.a_actual, self.a_actual], [0, 1.25*np.max(p_a_sum)], color = "black", lw = 0.5, ls = "--",
                       label = "Actual location")
        self.ax_a.set_ylim((0, 1.25*np.max(p_a_sum)))
        self.ax_a.legend(loc = "upper left")
        
        # Get the 1-d probability distribution over offshore distance
        p_b_sum = np.nansum(probability, axis = 0)
        self.ax_b.plot(p_b_sum, self.b, color = "darkgreen")
        self.ax_b.fill_betweenx(self.b, np.zeros(p_b_sum.shape), p_b_sum, color = "darkgreen", alpha = 0.25, label = "Marginal distribution for β")
        self.ax_b.plot([0, 1.25*np.max(p_b_sum)], [self.b_actual, self.b_actual], color = "black", lw = 0.5, ls = "--")
        self.ax_b.set_xlim((1.25*np.max(p_b_sum), 0))
        
        # Histogram of observed flashes
        self.ax_data.hist(self.observations, bins = np.linspace(self.a_min, self.a_max, 50), color = "darkseagreen")
        
        self.fig.text(0.12, 0.8, "Observations:\n{}".format(len(self.observations)),
                      fontsize = 14)
    
    def animate(self, observations, frame_duration_ms = 50, compress = False, **kwargs):
        frames = []
        frame = gif.frame(self.plot_probabilities)(show_actual = True, animated = True, **kwargs)
        frames.append(frame)
        for o_ind, observation in enumerate(observations):
            print("Animating: frame {}/{}".format(o_ind+1, len(observations)))
            
            self.feed_data(observation)
            frame = gif.frame(self.plot_probabilities)(show_actual = True, animated = True, **kwargs)
            frames.append(frame)
        
        gif.save(frames, "lighthouse.gif", duration = frame_duration_ms)
        
        if compress:
            compress_video()

def compress_video(filename = "lighthouse", extension = ".gif"):
    input_filename = filename + extension
    output_filename = filename + ".mp4"
    print("Compressing file to mp4")
    try:
        import ffmpy
        
        if ffmpeg_path is None:
            print("To create an mp4 file, you need to have the ffmpeg executable installed")
            print("and provide the path in the variable named 'ffmpeg_path'")
        ff = ffmpy.FFmpeg(executable=ffmpeg_path,
                          inputs = {input_filename: None},
                          outputs = {output_filename: None})
        ff.run()
    except ImportError:
        print("Failed to compress gif to mp4. Is ffmpy installed?")
    except TypeError:
        err_msg = "To output mp4 video, you need to specify a path to ffmpeg."
        err_msg += "\nCould not find ffmpeg at path {}".format(ffmpeg_path)
        print(err_msg)
        raise ValueError(err_msg)

def create_animated_gif(lighthouse, samples = 500, **kwargs):
    
    bonus_frames = 80
    frame_updates = np.ones(samples + bonus_frames, dtype = np.int64)
    
    # Pause on the first few frames, then animate the rest
    frame_updates[0:20] = 0
    frame_updates[21:41] = 0
    frame_updates[42:62] = 0
    frame_updates[63:83] = 0
    # Pause on the last frame
    frame_updates[-100:] = 0
    lighthouse.animate(frame_updates, compress = True, **kwargs)

def create_individual_plots(lighthouse):
    lighthouse.feed_data(num_data_points = 100)
    lighthouse.plot_probabilities(animated = False)
    plt.show()

if __name__ == "__main__":
    print("Start!")
    lighthouse = Lighthouse(a_actual = 31.4, b_actual = 27.2)
    
    create_individual_plots(lighthouse)
    #create_animated_gif(lighthouse)
    print("Done.")
    
