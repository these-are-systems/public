'''
Created on Jun. 7, 2022
These Are Systems

Copyright © 2022 Jacob Bayless Consulting Inc
Author: Jacob Bayless, P. Eng


Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the “Software”), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

"""
This file contains the source code accompanying the newsletter article here:
https://jbconsulting.substack.com/p/the-kalman-filter-on-a-moving-target
"""

import os
import numpy as np

import matplotlib
matplotlib.use("webAgg")
from matplotlib import pyplot as plt
from matplotlib.ticker import LogFormatter
import gif


from tas_public.articles.article_20220609 import Lighthouse, normalize_log, create_animated_gif, compress_video

rng = np.random.default_rng()


def color_interpolate(index, num_colors = 50):
    color = matplotlib.cm.get_cmap("viridis")(index/num_colors)
    return color
    

def offset_accumulate_sum(PP, v, t, delta_x):
    """
    Add up a probability distribution PP while shifting the probability mass
    in proportion to a velocity array
    """
    offset_inds = (v*t)/delta_x
    P = np.zeros(PP.shape[:-1])
    for v_ind, offset in enumerate(offset_inds):
        offset_f = int(np.floor(offset))
        offset_c = int(np.ceil(offset))
        offset_f_coeff = offset - offset_f
        offset_c_coeff = offset_c - offset
        
        if((offset_f > 0) and (offset_f < PP.shape[0]) and offset_f_coeff > 0.0):
            P[offset_f:,:] += offset_f_coeff*PP[:-offset_f,:,v_ind]
        elif((offset_f < 0) and (-offset_f < PP.shape[0])  and offset_f_coeff > 0.0):
            P[:offset_f,:] += offset_f_coeff*PP[-offset_f:,:,v_ind]
        elif((offset_f == 0) and offset_f_coeff > 0.0):
            P += offset_f_coeff*PP[:,:,v_ind]
        if((offset_c > 0) and (offset_c < PP.shape[0])  and offset_c_coeff > 0.0):
            P[offset_c:,:] += offset_c_coeff*PP[:-offset_c,:,v_ind]
        elif((offset_c < 0) and (-offset_c < PP.shape[0]) and offset_c_coeff > 0.0):
            P[:offset_c,:] += offset_c_coeff*PP[-offset_c:,:,v_ind]    
        elif((offset_c == 0) and offset_c_coeff > 0.0):
            P += offset_c_coeff*PP[:,:,v_ind]
    return P

def gaussian_model_ND(coordinates, means, covariances, **kwargs):
    means = np.array(means)
    VI = np.linalg.inv(covariances)
    dists = np.moveaxis(np.array(coordinates), 0, -1) - means.T
    
    d_mahalanobis_sq = np.einsum('abcj,ji,abci->abc', dists, VI, dists)
    
    gaussian = np.exp(-0.5*d_mahalanobis_sq)
    norm = 1./np.sum(gaussian)
    
    gaussian = norm*gaussian
    return gaussian


class Ship_Kalman(Lighthouse):
    def __init__(self,
                 a_actual = 0.0,
                 b_actual = 1.0,
                 v_actual = 1.0,
                 timestep_s = 1.0,
                 **kwargs):
        """
        Create a Ship problem instance
        
        Parameters:
        ----------
        a_actual : float, optional
            The true location of the ship along the shoreline, in km
            Default is 0 (the origin)
        b_actual : float, optional
            The true location of the ship along the shoreline, in km
            Default is 1 km offshore
        v_actual : float, optional
            The true velocity of the ship along the shoreline, in m/s
        timestep_s : float, optional
            The time, in seconds, between subsequent observations
        
        Additional keyword arguments passed to _init_bayes() to initialize
        the Bayes filter.
        """
        
        self.timestep_s = timestep_s
        self.v_units = self.timestep_s/1000.0
        self.a_actual = a_actual
        self.b_actual = b_actual
        self.v_actual = v_actual*self.v_units # convert from m/s to km/timestep
        self.observations = []
        
        self.gaussian_color = "goldenrod"
        self.gaussian_label = "Gaussian approximation"
        self.y_axis_label = "Distance into the sea (km)"
        self.y_axis_title = "Probability for log(β)\n(distance offshore)"
        
        self._init_bayes(**kwargs)
        self._init_kalman()
        
        self.generate_figure()
    
    def _init_bayes(self,
                 num_a_points = 201,
                 num_b_points = 200,
                 num_v_points = 601,
                 left_shoreline_extent_km = -100.,
                 right_shoreline_extent_km = 100.,
                 offshore_extent_km = 200.,
                 min_v_m_s = -600,
                 max_v_m_s = 600):
        """
        Initialize Bayes filter parameters
        
        Parameters
        ----------
        num_a_points : int, optional
            The number of grid points to use for discretizing the shoreline
            Default is 201
        num_b_points : int, optional
            The number of grid points to use for discretizing the ocean
            Default is 100
        num_v_points : int, optional
            The number of grid points to use for discretizing the velocity
            Default is 601
        left_shoreline_extent_km : float, optional
            Extent of the shoreline grid to the left, in km
            Default is -100
        right_shoreline_extent_km : float, optional
            Extent of the shoreline grid to the right, in km
            Default is 100
        offshore_extent_km : float, optional
            Extent of the ocean grid offshore, in km
            Default is 100
        min_v_m_s : float, optional
            Minimum plausible velocity, in m/s
            Default is -600
        max_v_m_s : float, optional
            Maximum plausible velocity, in m/s
            Default is 600
        """
        self.a_min = left_shoreline_extent_km
        self.a_max = right_shoreline_extent_km
        self.logb_min = np.log(1.0)
        self.logb_max = np.log(offshore_extent_km)
        self.b_min = self.logb_min
        self.b_max = self.logb_max
        self.v_min = min_v_m_s*self.v_units
        self.v_max = max_v_m_s*self.v_units
        
        
        a = np.linspace(self.a_min, self.a_max, num_a_points, endpoint = True)
        b = np.linspace(self.logb_min, self.logb_max, num_b_points, endpoint = False)
        v = np.linspace(self.v_min, self.v_max, num_v_points, endpoint = True)
    
        aa, bb, vv = np.meshgrid(a, b, v, indexing = 'ij')
        aa_proj, bb_proj = np.meshgrid(a, b, indexing = 'ij')
        
        self.delta_a = (a[-1] - a[0])/(num_a_points - 1)
        self.a = a
        self.logb = b
        self.v = v
        self.aa = aa
        self.logbb = bb
        self.vv = vv
        self.time = 0
        self.aa_proj = aa_proj
        self.logbb_proj = bb_proj
        
        # Jeffreys prior over b, uniform over a, gaussian over v
        v_prior_std_deviation = 250/self.v_units # 250 m/s in km/timestep
        log_prior = normalize_log(np.ones(self.logbb.shape) - 0.5*(vv**2)/(v_prior_std_deviation**2))
        
        self.log_probability = log_prior
        
        
    def _init_kalman(self):
        """
        Initialize Kalman filter parameters
        """
        
        self._x_prev = None
        self.a0_init = 0.0        
        self.b0_init = 1.0
        self.v0_init = 0.0 # measured in km/timestep
        
        self.kalman_mean = np.array([[self.a0_init],
                                     [np.log(self.b0_init)],
                                     [self.v0_init]])
        
        v0_cov = (250*self.v_units)**2 # standard deviation 250 m/s
        
        self.kalman_cov = np.array([[40000, 0.0, 0.0,],
                                    [0.0, 1000000.0, 0.0],
                                    [0.0,    0.0, v0_cov]])
        
        self.kalman_mean_forecast = self.kalman_mean
        self.kalman_cov_forecast = self.kalman_cov
        
        # For plotting the Kalman filter convergence trend
        self.kalman_mean_history = [np.array(self.kalman_mean)]
        self.kalman_cov_history = [np.array(self.kalman_cov)]
        
        self.h_adj = np.log(self.b0_init)
        self.b0_prev = self.b_actual
        
        self.gaussian_color = "lightpink"
        self.prediction_color = "red"
        self.gaussian_label = "Kalman filter"
        self.dropped_data = 0
        
    def generate_figure(self):
        Lighthouse.generate_figure(self)
        
        b_ticks = np.array([1, 2, 5, 10, 20, 50, 100, 200])
        self.ax_img.set_yticks(np.log(b_ticks))
        self.ax_img.set_yticklabels(b_ticks)
        
        self.ax_v = self.fig.add_subplot(self.gs[0,0])
        self.ax_v.set_yticklabels([])
        self.ax_v.set_yticks([])
        self.ax_v.set_title("Estimated velocity")
        self.ax_v.set_xlabel("m/s")
        self.ax_img.set_title("Estimated ship location (joint probability)")
        
    def generate_data(self):
        """
        Generate a new observed light flash along the shoreline at a random position
        
        Returns
        -------
        x_n : float
            Position of the light flash, in kilometres
        """
        theta = rng.uniform(-np.pi, np.pi)
        x_n = (self.a_actual + self.v_actual*self.time) + self.b_actual*np.tan(theta)
        self.observations.append(x_n)
        self.time += self.timestep_s
        return(x_n)    
    
    def feed_data(self, num_data_points = 1):
        """
        Generate a number of observations and update the probability distribution
        accordingly.
        
        Parameters
        ----------
        num_data_points : int
            The requested number of new observations to update on
        """
        if num_data_points == 0:
            return
        for _ in range(int(num_data_points)):
            x_n = self.generate_data()
            self.update_bayes(x_n)
            self.update_kalman(x_n)
    
    def update_bayes(self, x_n):
        """
        Update the probability distribution based on a single new observation
        
        Parameters
        ----------
        x_n : float
            Position of the new light flash, in kilometres
        """
        print("Time: {} ({:.0f})".format(self.time, self.time/self.timestep_s))
        
        log_likelihood = np.log((1./np.pi)*(np.exp(self.logbb)/(np.exp(self.logbb)**2 + (x_n - self.aa - self.vv*self.time)**2)))
        log_prior = self.log_probability
        log_posterior =  log_likelihood + log_prior
        self.log_probability = np.nan_to_num(log_posterior, copy = False, nan = -np.inf)
        self.log_probability = normalize_log(self.log_probability)
    
    
    def update_kalman(self, x_n):
        self.predict_kalman()
        self._update_kalman(x_n)
    
    def predict_kalman(self):
        """
        Prepare the Kalman estimate probability distribution for the next timestep
        """
        
        # State transition matrix
        F = np.array([[1, 0, self.timestep_s],
                      [0, 1, 0              ],
                      [0, 0, 1              ]])
        
        self.kalman_mean_forecast = np.matmul(F, self.kalman_mean)
        self.kalman_cov_forecast = np.matmul(F, np.matmul(self.kalman_cov, F.T))
    
    def _update_kalman(self, x_n):
        """
        Update the Kalman estimate probability distribution based on a single new observation
        
        Parameters
        ----------
        x_n : float
            Position of the new light flash, in kilometres
        """
        print("############### UPDATE {} ##############".format(len(self.observations)))
        
        h_b = 1
        if self._x_prev is None:
            self._x_prev = x_n
            h_b = 0
        
        a0_prev = self.kalman_mean[0,0]
        a0_forecast = self.kalman_mean_forecast[0,0]
        b0_forecast = np.exp(self.kalman_mean_forecast[1,0])
        abs_error = np.abs(x_n - a0_forecast)
        v_meas_n = (x_n - a0_prev)
        
        x = np.array([[x_n],
                      [np.nan_to_num(np.log(abs_error), nan = -100, neginf = -100, posinf = 100)],
                      [v_meas_n]])
        
        self.h_adj += (x[1,0] - self.h_adj)/len(self.observations)
        
        print("New measurement vector: {}".format(x.T))
        
        h_a = 1
        h_v = 1
        if (np.abs(x_n - a0_forecast) > b0_forecast*2.798) and (h_b > 0):
            print("Discarding observation as outlier")
            self.dropped_data += 1
            h_a = 0
            h_v = 0
        
        P_k = self.kalman_cov_forecast # Covariance: uncertainty of the estimate
        s_aa2 = self.kalman_cov_forecast[0,0]
        s_bb2 = self.kalman_cov_forecast[1,1]
        s_bb = np.sqrt(s_bb2)
        
        
        H_k = np.array([[h_a, 0, 0],
                        [0, h_b, 0],
                        [0, 0, h_v]])
        
        mod_x = (1+s_bb)
        R_k_22 = -0.5*mod_x*b0_forecast**2
        
        R_k = np.array([[mod_x*b0_forecast**2,    0,   R_k_22],
                        [0,  np.log(1. + np.sqrt(s_aa2)/b0_forecast) + (0.5*np.pi)**2 , 0],
                        [R_k_22,   0,   s_aa2 + mod_x*b0_forecast**2]])
        
        K_k = np.matmul(P_k, H_k.T)*\
                    (np.linalg.inv(np.matmul(H_k,
                              np.matmul(P_k, H_k.T))+R_k))
        K_k = np.nan_to_num(K_k)
        
        ### State update equation
        self.kalman_mean = self.kalman_mean_forecast + np.matmul(K_k, (x - np.matmul(H_k, self.kalman_mean_forecast)))
        self.kalman_cov = np.matmul((np.eye(3) - np.matmul(K_k, H_k)),
                                    np.matmul(P_k,
                                      (np.eye(3) - np.matmul(K_k, H_k)).T))\
                        + np.matmul(K_k,
                                    np.matmul(R_k, K_k.T))
        
        print("Updated mean:\n {}".format(self.kalman_mean))
        print("Updated covariance:\n {}".format(self.kalman_cov))
        self.kalman_mean_history.append(np.array(self.kalman_mean))
        self.kalman_cov_history.append(np.array(self.kalman_cov))
        
        self.b0_prev = b0_forecast
        self._x_prev = x_n
    
    def gaussian_approx(self, forecast = False, **kwargs):
        if forecast:
            self.gaussian_fit = [self.kalman_mean_forecast, self.kalman_cov_forecast]
        else:
            self.gaussian_fit = [self.kalman_mean, self.kalman_cov]
    
    def plot_kalman_history(self):
        """
        Plots a history of the Kalman filter mean and covariance, illustrating its
        convergence to the truth over time 
        """
        self.fig_coords = plt.figure(figsize = (8,6))
        self.gs_coords = self.fig_coords.add_gridspec(3, 1)
        
        ax_a = self.fig_coords.add_subplot(self.gs_coords[0,0])
        ax_b = self.fig_coords.add_subplot(self.gs_coords[1,0])
        ax_v = self.fig_coords.add_subplot(self.gs_coords[2,0])
        
        ax_a.set_title("With prediction, the Kalman filter parameter estimate converges")
        ax_a.set_ylabel("Estimated\nshoreline position")
        ax_b.set_ylabel("Estimated\ndistance from shore")
        ax_v.set_ylabel("Estimated\nvelocity")
        ax_v.set_xlabel("Number of observations")
        ax_a.grid()
        ax_b.grid()
        ax_v.grid()
        
        kalman_mean_history = np.array(self.kalman_mean_history)
        t = np.arange(0, kalman_mean_history.shape[0])
        a_mean = kalman_mean_history[:,0,0]
        b_mean =  kalman_mean_history[:,1,0]
        v_mean =  kalman_mean_history[:,2,0]
        ax_a.plot(t, a_mean)
        ax_b.plot(t, np.exp(b_mean))
        ax_v.plot(t, v_mean/self.v_units)
        
        cov = np.array(self.kalman_cov_history)
        
        s_aa = cov[:,0,0]
        s_bb = cov[:,1,1]
        s_vv = cov[:,2,2]
        
        ax_a.fill_between(t, a_mean - np.sqrt(s_aa), a_mean + np.sqrt(s_aa), alpha = 0.25)
        
        ax_b.fill_between(t, np.exp(b_mean - np.sqrt(s_bb)), np.exp(b_mean + np.sqrt(s_bb)), alpha = 0.25)
        ax_b.set_yscale("log")
        formatter = LogFormatter(labelOnlyBase=False, minor_thresholds=(2, 0.4))
        ax_b.get_yaxis().set_minor_formatter(formatter)
        ax_b.get_yaxis().set_major_formatter(formatter)
        ax_v.fill_between(t, v_mean/self.v_units - np.sqrt(s_vv)/self.v_units, v_mean/self.v_units + np.sqrt(s_vv)/self.v_units, alpha = 0.25)
        
        ax_a.set_ylim(np.min([self.a_actual - 20, self.a0_init - 10, self.a_actual+self.v_actual*np.max(t) - 20]),
                      np.max([self.a_actual + 50, self.a0_init + 10, self.a_actual+self.v_actual*np.max(t) + 50]))
        ax_v.set_ylim(np.min([self.v_actual/self.v_units - 20, self.v0_init/self.v_units - 10]),
                      np.max([self.v_actual/self.v_units + 50, self.v0_init/self.v_units + 10]))
        ax_b.set_ylim(self.b_actual*0.05, self.b_actual*20)
        ax_a.set_xlim(-1,np.max(t))
        ax_b.set_xlim(-1,np.max(t))
        ax_v.set_xlim(-1,np.max(t))
        ax_b.plot(t, self.b_actual*np.ones(t.shape), ls = "--", color = "black")
        ax_a.plot(t, self.a_actual*np.ones(t.shape) + self.v_actual*t, ls = "--", color = "black")
        ax_v.plot(t, self.v_actual*(1/self.v_units)*np.ones(t.shape), ls = "--", color = "black")
    
    
    def plot_probabilities(self, show_actual = True, animated = True, gaussian_approx = True,
                           total_measurements = None, forecast = False, **kwargs):
        if gaussian_approx:
            self.gaussian_approx(forecast = forecast, **kwargs)
            
            if forecast:
                gaussian_color = self.prediction_color
            else:
                gaussian_color = self.gaussian_color
        if animated:
            # gif library seems to require the figure is regenerated each frame
            self.generate_figure()
        
        probability = np.exp(self.log_probability)
        
        
        probability_compress = offset_accumulate_sum(probability, self.v, self.time, self.delta_a)
        probability_max_compress = np.max(probability_compress)
       
        
        # Plot the joint probability distribution
        self.ax_img.pcolormesh(self.aa_proj, self.logbb_proj, probability_compress,
                                    vmin = 0.0, vmax = probability_max_compress,
                                    shading = 'gouraud')
        
        if show_actual:
            # Mark the true lighthouse position with a white circle
            self.ax_img.scatter([self.a_actual+self.v_actual*self.time], [np.log(self.b_actual)], marker = 'o',
                                edgecolors = "white", facecolors  = "none")
            
            # Plot a trail
            if(np.abs(self.time*self.v_actual) > 3.0):
                self.ax_img.plot([self.a_actual,
                                  self.a_actual + self.v_actual*self.time
                                   - 3.0*np.sign(self.v_actual)],
                             [np.log(self.b_actual), np.log(self.b_actual)],
                                color = "white", lw = 0.5)
        
        if gaussian_approx:
            gaussian_probs = gaussian_model_ND([self.aa, self.logbb, self.vv], *list(self.gaussian_fit), ravel = False)
            # Compress to 2D
            gaussian_probs_ab = np.sum(gaussian_probs, axis = 2)
            max_gauss = np.max(gaussian_probs_ab)
            contours = np.array([max_gauss/8.0, max_gauss/2.0])
            self.ax_img.contour(self.aa_proj, self.logbb_proj, gaussian_probs_ab, contours, colors = gaussian_color,
                                linestyles = "--", linewidths = 0.75)
        
        # Get the 1-d probability distribution over shoreline position
        p_a_sum = np.nansum(probability_compress, axis = 1)
        self.ax_a.plot(self.a, p_a_sum, color = "darkgreen", label = "Marginal\ndistribution")
        self.ax_a.fill_between(self.a, np.zeros(self.a.shape), p_a_sum, color = "darkgreen", alpha = 0.25)
        
        if gaussian_approx:
            p_a_gauss_sum = np.sum(gaussian_probs, axis = (1,2))
            
            self.ax_a.plot(self.a, p_a_gauss_sum, color = gaussian_color, ls = "--", label = self.gaussian_label)
        
        self.ax_a.plot([self.a_actual+self.v_actual*self.time,
                        self.a_actual+self.v_actual*self.time],
                       [0, 1.25*np.max(p_a_sum)],
                       color = "black", lw = 0.5, ls = "--",
                       label = "Actual\nlocation")
        self.ax_a.set_ylim((0, 1.25*np.max(p_a_sum)))
        self.ax_a.legend(loc = "upper left")
        
        # Get the 1-d probability distribution over offshore distance
        p_b_sum = np.nansum(probability_compress, axis = 0)
        if gaussian_approx:
            p_b_gauss_sum = np.sum(gaussian_probs, axis = (0,2))
        self.ax_b.fill_betweenx(self.logb, np.zeros(p_b_sum.shape), p_b_sum, color = "darkgreen", alpha = 0.25, label = "Marginal\ndistribution for β")
        self.ax_b.plot(p_b_sum, self.logb, color = "darkgreen")
        if gaussian_approx:
            self.ax_b.plot(p_b_gauss_sum, self.logb, color = gaussian_color, ls = "--", label = self.gaussian_label)
        self.ax_b.plot([0, 1.25*np.max(p_b_sum)], [np.log(self.b_actual), np.log(self.b_actual)], color = "black", lw = 0.5, ls = "--")
        self.ax_b.set_xlim((1.25*np.max(p_b_sum), 0))
        
        
        # Get the 1-d probability distribution over velocity
        v_scaling = 1/self.v_units
        p_v_sum = np.nansum(probability, axis = (0,1))
        self.ax_v.fill_between(self.v*v_scaling, np.zeros(p_v_sum.shape), p_v_sum, color = "darkgreen", alpha = 0.25, label = "Marginal distribution for v")
        self.ax_v.plot(self.v*v_scaling, p_v_sum, color = "darkgreen")
        
        if gaussian_approx:
            p_v_gauss_sum = np.sum(gaussian_probs, axis = (0,1))
            self.ax_v.plot(self.v*v_scaling, p_v_gauss_sum, color = gaussian_color, ls = "--", label = self.gaussian_label)
        self.ax_v.plot([self.v_actual*v_scaling, self.v_actual*v_scaling],
                       [0, 1.25*np.max(p_v_sum)], color = "black", lw = 0.5, ls = "--")
        self.ax_v.set_ylim((0, 1.25*np.max(p_v_sum)))
        
        # Histogram of observed flashes
        self.temporal_histogram(total_measurements = total_measurements)
        
        fig_text = "Observations: {}".format(len(self.observations))
        
        if self.dropped_data is not None:
            if len(self.observations) > 0:
                fig_text += ",   Dropped: {} ({:.0f}%)".format(self.dropped_data, 100*self.dropped_data/len(self.observations))
            else:
                fig_text += ",   Dropped: {}".format(self.dropped_data)
        self.fig.text(0.12, 0.01, fig_text,
                      fontsize = 14)
    
    def temporal_histogram(self, total_measurements = None, by_recency = True):
        
        bins = np.linspace(self.a_min, self.a_max, 50, endpoint = True)
        dx = (bins[-1] - bins[0])/49
        bottoms = np.zeros(bins.shape)
        
        if total_measurements is None:
            total_measurements = len(self.observations)
        else:
            if (total_measurements < len(self.observations)):
                total_measurements = len(self.observations)
        
        # Bin observations
        for obs_ind, observation in enumerate(self.observations):
            if ((observation < self.a_min) or (observation > self.a_max)):
                continue
            
            if (by_recency and total_measurements is not None):
                color_ind =  obs_ind + total_measurements - len(self.observations)
            else:
                color_ind = obs_ind
            
            color = color_interpolate(color_ind, total_measurements)
            
            bar_bin = np.searchsorted(bins, observation)
            arr = np.zeros(bins.shape)
            arr[bar_bin] = 1
            self.ax_data.bar(bins, arr, dx, bottom = bottoms, color = color, edgecolor = None, label = None, align = "center")
            bottoms += arr
    
    def animate(self, observations, frame_duration_ms = 100, compress = False, **kwargs):
        frames = []
        frame = gif.frame(self.plot_probabilities)(show_actual = True, animated = True, **kwargs)
        frames.append(frame)
        for o_ind, observation in enumerate(observations):
            print("Animating: frame {}/{}".format(o_ind+1, len(observations)))
            self.feed_data(observation)
            frame = gif.frame(self.plot_probabilities)(show_actual = True, forecast = True, **kwargs)
            frames.append(frame)
            frame = gif.frame(self.plot_probabilities)(show_actual = True, forecast = False, **kwargs)
            frames.append(frame)
        
        gif.save(frames, "ship_kalman.gif", duration = frame_duration_ms)
        
        if compress:
            compress_video(filename = "ship_kalman")
    

def create_individual_plots(ship, data_points = 50,
                            gaussian_approx = False, **kwargs):
    ship.feed_data(num_data_points = data_points)
    ship.plot_probabilities(animated = False, gaussian_approx = gaussian_approx, **kwargs)
    ship.plot_kalman_history()

if __name__ == "__main__":
    print("Start!")
    
    a_actual = 31.4
    travel_distance = -2*a_actual
    samples = 500
    timestep_s = 1
    v_actual = 1000*travel_distance/(samples*timestep_s)
    if(v_actual < -400):
        v_actual = -400
    if(v_actual > 400):
        v_actual = 400
    print("Using velocity: {}".format(v_actual))
    
    ship = Ship_Kalman(a_actual = a_actual,
                       b_actual = np.exp(2.5),
                       v_actual = v_actual,
                       timestep_s = timestep_s)
    
    create_individual_plots(ship, data_points = 500, gaussian_approx = True, total_measurements = samples)
    #create_animated_gif(ship, samples = samples, gaussian_approx = True, predict = True, total_measurements = samples)
    plt.show()
    print("Done.")
